package nl.utwente.di.bookQuote;

public class CelsiusToFahrenheitConverter {
    public double celsiusToFahrenheit(double celsius) {
        return (celsius * 9.0 / 5.0) + 32;
    }
}
